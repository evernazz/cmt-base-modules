#include "Base/Modules/interface/correctionWrapper.h"
#include <iostream>

MyCorrections::MyCorrections() {}

MyCorrections::MyCorrections(std::string filename, std::string correction_name) {
  if (filename == "None" || correction_name == "None") {} // allow for dummy
  else {
    auto csetEl = correction::CorrectionSet::from_file(filename);
    SF_ = csetEl->at(correction_name);
  }
}

struct make_string_functor {
  std::string operator()(const std::string &x) const { return x; }
  std::string operator()(int x) const { return std::to_string(x); }
  std::string operator()(double x) const { return std::to_string(x); }
};

double MyCorrections::eval(const std::vector<correction::Variable::Type>& values) {
  try {
    return SF_->evaluate(values);
  } catch (std::exception const& e) {
    std::cerr << "#################### ERROR : Correctionlib evalutaion failed " << e.what() << std::endl;
    std::cerr << "while evaluating " << SF_->name() << " with input variables "; 
    for (auto const& variable : SF_->inputs())
      std::cerr << variable.name() << "(" << variable.description() << "), "; 
    std::cerr << std::endl << "Input values : ";
    for (auto const val : values) {
      std::cerr << std::visit(make_string_functor(), val) << ", ";
    }
    std::cerr << std::endl;
    throw;
  }
}
