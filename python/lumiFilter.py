def lumiFilterRDF(**kwargs):
    try:
        from Base.Filters.lumiFilter import lumiFilterRDF as lumiFilterRDF_new
        return lumiFilterRDF_new(**kwargs)
    except ModuleNotFoundError:
        raise ModuleNotFoundError(
            "\n" + 100 * "*" +\
            "\nThe implementation of the lumiFilterRDF module has been moved to "
            "https://gitlab.cern.ch/cms-phys-ciemat/event_filters. Please download this package into"
            "Base/Filters, recompile, and try again.\n"
            "In any case, it's preferrable to use the lumiFilterRDF module inside"
            "Base.Filters instead." + 100 * "*")
